"""
Load .npy and .csv datasets files
"""
import configparser
import os

import numpy as np
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
import pandas as pd

# Supress tf logging
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# Archivo de configuración
config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
config.read(os.path.join(path, 'settings.cfg'))


def load_csv(num_classes, fd_id_equipo='all', test_size=0.1):
    columnas = ["P", "L_O", "L_T", "L_MO", "L_POT", "L_ME", "L_DE", "L_P", "V_O", "V_T", "V_MO", "V_POT", "V_ME",
                "V_DE", "V_P", "R"]

    files = [config['PATHS']['csv_file'].format(temporada=i, fd_id_equipo=fd_id_equipo,
                                                num_res=num_classes) for i in ["17-18", "18-19", "19-20"]]

    # Concatenar varios csv en un mismo dataframe
    # https://stackoverflow.com/a/21232849
    tmp = []
    for t, f in enumerate(files, start=1):
        print(f"\r[*] Cargando temporada: {t}/{len(files)}", end="")
        df = pd.read_csv(f, index_col='P', header=0, names=columnas, dtype=np.float64)
        tmp.append(df)

    df = pd.concat(tmp, axis=0, ignore_index=True, names=columnas)
    df.R.astype(np.int8)

    X = df.loc[:, columnas[1:-1]].values
    y = df[columnas[-1]].values.astype(int).ravel()
    y = to_categorical(y)

    # Separar en train y test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)

    print(f"\nTrain shape: {X_train.shape}")
    print(f"Test shape: {X_test.shape}")

    return X_train, X_test, y_train, y_test


def load_npz(num_matches, num_classes, fd_id_equipo='all', flattened=False, test_size=0.1):
    files = [config['PATHS']['npz_file'].format(temporada=i, fd_id_equipo=fd_id_equipo,
                                                num_res=num_classes) for i in ["17-18", "18-19", "19-20"]]

    if flattened:
        X = np.empty((num_matches, 22 * 34), dtype=np.float16)
    else:
        X = np.zeros((num_matches, 22, 34, 1), dtype=np.float16)
    y = np.zeros(num_matches, dtype=np.uint8)

    i = 0
    for t, f in enumerate(files, start=1):
        print(f"\r[+] Cargando temporada: {t}/{len(files)}", end="")

        tmp = np.load(f)
        length = tmp['stats'].shape[0]
        if flattened:
            X[i:i + length, :] = np.reshape(tmp['stats'], (length, 22 * 34))
        else:
            X[i:i + length, :, :, :] = np.reshape(tmp['stats'], (length, 22, 34, 1))
        y[i:i + length] = tmp['results']
        i += length

    y = to_categorical(y)

    # Separar en train y test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)

    print(f"\nTrain shape: {X_train.shape}")
    print(f"Test shape: {X_test.shape}")

    return X_train, X_test, y_train, y_test


if __name__ == '__main__':
    # load_npz(2908, 2)
    load_csv(2)

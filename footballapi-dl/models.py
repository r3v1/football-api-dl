import tensorflow.keras as keras


def build_model(num_classes):
    # Create model
    model = keras.Sequential()

    # 1st conv layer
    model.add(keras.layers.Conv2D(64, kernel_size=(22, 1), activation='relu', strides=1, padding='same',
                                  input_shape=(22, 34, 1)))
    # model.add(keras.layers.MaxPooling2D(pool_size=(1, 2)))
    model.add(keras.layers.BatchNormalization())

    # 2nd conv layer
    model.add(keras.layers.Conv2D(64, kernel_size=(1, 17), activation='relu', strides=1, padding='same'))
    # model.add(keras.layers.MaxPooling2D(pool_size=(1, 2)))
    model.add(keras.layers.BatchNormalization())

    # 3rd conv layer
    model.add(keras.layers.Conv2D(32, kernel_size=(11, 1), activation='relu', strides=1, padding='same'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    model.add(keras.layers.BatchNormalization())

    # 4th conv layer
    model.add(keras.layers.Conv2D(8, kernel_size=(5, 1), activation='relu', strides=1, padding='same'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    model.add(keras.layers.BatchNormalization())

    # Flatten
    model.add(keras.layers.Flatten())

    # Dense
    model.add(keras.layers.Dense(64, activation='relu'))
    model.add(keras.layers.Dropout(0.25))

    # Dense
    model.add(keras.layers.Dense(32, activation='relu'))
    # model.add(keras.layers.Dropout(0.3))

    # Output
    model.add(keras.layers.Dense(num_classes, activation='softmax'))

    return model


def build_unai(num_classes):
    # Create model
    model = keras.Sequential()

    # 1st conv layer
    model.add(keras.layers.Conv2D(64, kernel_size=(3, 1), activation='relu', strides=1, padding='valid',
                                  input_shape=(22, 34, 1)))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    model.add(keras.layers.BatchNormalization())

    # # 2nd conv layer
    model.add(keras.layers.Conv2D(32, kernel_size=(1, 4), activation='relu', strides=1, padding='valid'))
    model.add(keras.layers.MaxPooling2D(pool_size=(1, 2)))
    model.add(keras.layers.BatchNormalization())

    # Flatten
    model.add(keras.layers.Flatten())

    # Dense
    model.add(keras.layers.Dense(64, activation='relu'))
    model.add(keras.layers.Dropout(0.45))

    # Dense
    model.add(keras.layers.Dense(8, activation='relu'))
    model.add(keras.layers.Dropout(0.05))

    # Output
    model.add(keras.layers.Dense(num_classes, activation='softmax'))

    return model


def build_mlp(input_shape, num_classes):
    """
    Multilayer Perceptron para el dataset csv de 14 variables predictoras
    y la clase a predecir.

    Parameters
    ----------
    input_shape: tuple
        Tamaño de la entrada
    num_classes: int
        Número de clases a predecir, 2 o 3

    Notes
    -----
    Tiene una precisión aproximada del ~60%, la mejor vista ha sido del 65%

    Returns
    -------
    tf.keras.model
    """
    model = keras.Sequential()

    model.add(keras.layers.Dense(4096, input_shape=input_shape, activation='relu'))
    # model.add(keras.layers.Dropout(0.4))
    model.add(keras.layers.BatchNormalization())

    model.add(keras.layers.Dense(512, activation='relu'))
    # model.add(keras.layers.Dropout(0.5))
    model.add(keras.layers.BatchNormalization())

    model.add(keras.layers.Dense(32, activation='relu'))
    # model.add(keras.layers.Dropout(0.05))
    model.add(keras.layers.BatchNormalization())

    model.add(keras.layers.Dense(num_classes, activation='relu'))

    return model


if __name__ == '__main__':
    import numpy as np
    import os

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    X = np.random.random((380, 22, 34, 1))
    y = np.random.randint(3, size=380)

    print(X.shape)
    print(y.shape)

    # model = ADFNet(3)
    # model.build((None, 22, 34, 1))

    model = build_model(3)
    # model = build_model2(3)

    model.summary()
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    model.fit(X, y, epochs=10, validation_split=0.1)

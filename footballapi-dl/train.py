import sys

import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import KFold
from tensorflow.keras.callbacks import ModelCheckpoint

from datasets import load_csv, load_npz
from models import *


def build(flattened=False):
    # Build model
    if flattened:
        # Build MLP (DL)
        model = build_mlp((748,), num_classes)
    else:
        # Build CNN-1
        model = build_model(num_classes)

        # Build CNN-2
        # model = build_unai(num_classes)

        # Build MLP (ML)
        # model = build_mlp((14,), num_classes)
    # model.summary()

    # Compile model
    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=0.001),
        loss='binary_crossentropy' if num_classes == 2 else 'categorical_crossentropy',
        metrics=['accuracy']
    )

    return model


def train(dataset, num_classes, epochs, flattened=False, cv=False):
    X_train, X_test, y_train, y_test = dataset

    if cv:
        X = np.concatenate((X_train, X_test))
        y = np.concatenate((y_train, y_test))
        history = []

        kf = KFold(n_splits=10)

        for i, (train_idx, test_idx) in enumerate(kf.split(X, y)):
            X_train, X_test = X[train_idx], X[test_idx]
            y_train, y_test = y[train_idx], y[test_idx]

            model = build(flattened=flattened)

            # Fit model
            print(f"\r[{i:>2}] Entrenando {epochs} epochs...", end="")
            h = model.fit(
                X_train, y_train,
                validation_data=(X_test, y_test),
                batch_size=32, epochs=epochs,
                verbose=0
            )

            history.append(h.history)

            print(f"\r[{i:>2}] mean: {np.mean(h.history['val_accuracy']):.4f} {' ' * 20}")

        model.summary()

        print(f"mean: {np.mean([np.mean(h['val_accuracy']) for h in history]):.4f} "
              f"std: {np.mean([np.std(h['val_accuracy']) for h in history]):.4f} "
              f"max: {np.max([h['val_accuracy'] for h in history]):.4f} "
              f"min: {np.min([h['val_accuracy'] for h in history]):.4f} ")
    else:
        model = build(flattened=flattened)

        # Fit model
        history = model.fit(
            X_train, y_train,
            validation_data=(X_test, y_test),
            batch_size=32, epochs=epochs,
            callbacks=[
                ModelCheckpoint(f"adfnet_{num_classes}" + "_{val_accuracy}.h5", monitor='val_accuracy',
                                save_best_only=True,
                                save_weights_only=False)
            ]
        )

        plt.plot(history.history['accuracy'], label='accuracy')
        # plt.plot(history.history['loss'], label='loss')
        plt.plot(history.history['val_accuracy'], label='val_accuracy')
        # plt.plot(history.history['val_loss'], label='val_loss')
        plt.title("Train history")
        plt.tight_layout()
        plt.legend(loc="upper left")
        plt.show()


if __name__ == '__main__':
    if len(sys.argv[1:]):
        epochs = int(sys.argv[1])
    else:
        epochs = 25

    num_classes = 2
    flattened = False
    dataset = load_npz(num_matches=3702, num_classes=num_classes, flattened=flattened)
    # dataset = load_csv(num_classes)
    train(dataset, num_classes, epochs=epochs, flattened=flattened, cv=False)
